;;; helm-ykoath.el --- A helm interface to yubikey OATH -*- lexical-binding: t -*-

;; Copyright (C) 2019 Raffael Stocker

;; Author: Raffael Stocker <r.stocker@mnet-mail.de>
;; Maintainer: Raffael Stocker <r.stocker@mnet-mail.de>
;; Created: 27. Okt 2019
;; Version: 0.1
;; Package-Version:
;; Package-Requires: ((emacs "24"))
;; Keywords: hardware
;; URL: https://gitlab.com/rstocker/helm-ykoath

;; This file is NOT part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:
;; This package uses the helm interface to select one of the credentials
;; stored on a YubiKey.  It then types the OATH pin into the buffer at
;; point or into the focussed X window, if exwm is used.

;;; Code:

(require 'helm)
(require 'exwm)
(require 'exwm-input)

(defgroup helm-ykoath nil
  "Emacs helm interface for YubiKey OATH."
  :group 'helm)

(defcustom helm-ykoath-program
  (executable-find "ykman")
  "Path of the 'ykman' program used to access the YubiKey."
  :type 'string)

(defcustom helm-ykoath-actions
  '(("Type OATH pin into active window" . helm-ykoath-type))
  "List of actions for `helm-ykoath'."
  :group 'helm-ykoath
  :type '(alist :key-type string :value-type function))

(defvar helm-ykoath-sources
  (helm-build-sync-source "Account"
    :candidates #'helm-ykoath-list
    :action helm-ykoath-actions))

(defun helm-ykoath-type (entry)
  "Get OATH credentials for ENTRY."
  (ignore-errors
    (let* ((secret (car (last (split-string
			       (shell-command-to-string
				(format "%s oath code %s 2>/dev/null"
					helm-ykoath-program
					entry)))))))
      (if (exwm--buffer->id (current-buffer))
	  (let ((exwm-debug nil))
	    (mapc #'exwm-input--fake-key secret))
	(insert secret))
      (clear-string secret))))

(defun helm-ykoath-list ()
  "Get available OATH accounts."
  (condition-case err
      (split-string (shell-command-to-string
		     (format "%s oath list 2>/dev/null" helm-ykoath-program))
		    "\n")
    (error (message "%s" (error-message-string err))
	   '())))

;;;###autoload
(defun helm-ykoath ()
  "Helm interface to YubiKey OATH."
  (interactive)
  (helm :sources 'helm-ykoath-sources
	:buffer "*helm-ykoath*"))

(provide 'helm-ykoath)

;;; helm-ykoath.el ends here
